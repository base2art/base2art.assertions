﻿using System;
namespace Base2art.Assertions
{
    public class EqualObject_BadImplementation : IEquatable<EqualObject_BadImplementation>
    {
        public string Value { get; set; }
        
        public override bool Equals(object obj)
        {
            EqualObject_BadImplementation other = obj as EqualObject_BadImplementation;
            return this.Equals(other);
        }

        public bool Equals(EqualObject_BadImplementation other)
        {
            if (other == null)
            {
                return false;
            }
            
            return this.Value == other.Value;
        }
        
        public override int GetHashCode()
        {
            int hashCode = 0;
            unchecked 
            {
                if (Value != null)
                {
                    hashCode += 1000000007 * Value.GetHashCode();
                }
            }
            return hashCode;
        }

        public static bool operator ==(EqualObject_BadImplementation lhs, EqualObject_BadImplementation rhs) 
        {
            if (ReferenceEquals(lhs, rhs))
            {
                return true;
            }
            
            if (ReferenceEquals(lhs, null) || ReferenceEquals(rhs, null))
            {
                return false;
            }
            
            return lhs.Equals(rhs);
        }

        public static bool operator !=(EqualObject_BadImplementation lhs, EqualObject_BadImplementation rhs) 
        {
            return !(lhs == rhs);
        }
    }
}
