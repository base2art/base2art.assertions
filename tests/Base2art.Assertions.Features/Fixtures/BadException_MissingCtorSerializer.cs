﻿namespace Base2art.Assertions.Features
{
	using System;
	using System.Runtime.Serialization;

	[Serializable]
	public class BadException_MissingCtorSerializer : Exception
	{
		public BadException_MissingCtorSerializer()
		{
		}

		public BadException_MissingCtorSerializer(string message) : base(message)
		{
		}

		public BadException_MissingCtorSerializer(string message, Exception innerException) : base(message, innerException)
		{
		}

		// This constructor is needed for serialization.
//		protected BadException_MissingCtorSerializer(SerializationInfo info, StreamingContext context) : base(info, context)
//		{
//		}
	}
}





