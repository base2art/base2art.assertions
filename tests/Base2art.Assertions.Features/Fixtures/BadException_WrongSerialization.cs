﻿namespace Base2art.Assertions.Features
{
	using System;
	using System.Runtime.Serialization;

	[Serializable]
	public class BadException_WrongSerialization : Exception, ISerializable
	{
	    public int CustomData { get; set; }
	    
		public BadException_WrongSerialization()
		{
		}

		public BadException_WrongSerialization(string message) : base(message)
		{
		}

		public BadException_WrongSerialization(string message, Exception innerException) : base(message, innerException)
		{
		}

		// This constructor is needed for serialization.
		protected BadException_WrongSerialization(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
		
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("CustomData", this.CustomData, typeof(int));
        }
	}
	
}







