﻿namespace Base2art.Assertions.Fixtures
{
	using System;

	public class ObjThatMissesAnItemExceptionNull
	{
		public ObjThatMissesAnItemExceptionNull(string str1, string str2)
		{
            if (str1 == null)
            {
                throw new ArgumentNullException("str1");
            }
		}
	}

	public class ObjThatHasStructs
	{
		public ObjThatHasStructs(string str1, int int1, int? nint1)
		{
            if (str1 == null)
            {
                throw new ArgumentNullException("str1");
            }
		}
	}
}




