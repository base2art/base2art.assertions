﻿namespace Base2art.Assertions.Features
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public class BadException_MissingCtorOther : Exception
    {
        public BadException_MissingCtorOther()
        {
        }

        public BadException_MissingCtorOther(string message) : base(message)
        {
        }

//        public BadException_MissingCtorOther(string message, Exception innerException) : base(message, innerException)
//        {
//        }
        
        // This constructor is needed for serialization.
        protected BadException_MissingCtorOther(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
    
}





