﻿namespace Base2art.Assertions.Features
{
	using System;
	using System.Runtime.Serialization;

	[Serializable]
	public class BadException_WrongSerialization2 : Exception, ISerializable
	{
		public int CustomData {
			get;
			set;
		}

		public BadException_WrongSerialization2()
		{
		}

		public BadException_WrongSerialization2(string message) : base(message)
		{
		}

		public BadException_WrongSerialization2(string message, Exception innerException) : base(message, innerException)
		{
		}

		// This constructor is needed for serialization.
		protected BadException_WrongSerialization2(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}









