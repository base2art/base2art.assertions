﻿namespace Base2art.Assertions.Features
{
    using System;
    using System.Runtime.Serialization;
    
//    [Serializable]
    public class BadException_NotSerializable : Exception, ISerializable
    {
        public BadException_NotSerializable()
        {
        }

        public BadException_NotSerializable(string message) : base(message)
        {
        }

        public BadException_NotSerializable(string message, Exception innerException) : base(message, innerException)
        {
        }

        // This constructor is needed for serialization.
        protected BadException_NotSerializable(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}

