﻿namespace Base2art.Assertions.Fixtures
{
    using System.Runtime.Serialization.Formatters;

    public class ObjWithArgCheckingInterface
    {
        public ObjWithArgCheckingInterface(IFieldInfo abc)
        {
            if (abc == null)
            {
                throw new System.ArgumentNullException("abc");
            }
            
		    this.Items = abc.FieldNames;
        }
        
        public string[] Items { get; set; }
    }
    
}


