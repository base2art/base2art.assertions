﻿namespace Base2art.Assertions.Features
{
	using System;
	using System.Runtime.Serialization;

	[Serializable]
	public class BadException_MissingCtor : Exception
	{
		public BadException_MissingCtor()
		{
		}

		//		public BadException_MissingCtor(string message) : base(message)
		//		{
		//		}
		
		public BadException_MissingCtor(string message, Exception innerException) : base(message, innerException)
		{
		}

		// This constructor is needed for serialization.
		protected BadException_MissingCtor(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}





