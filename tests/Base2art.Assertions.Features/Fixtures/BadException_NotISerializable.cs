﻿namespace Base2art.Assertions.Features
{
	using System;
	using System.Runtime.Serialization;

	[Serializable]
	public class BadException_MissingDefaultCtor : Exception
	{
		public BadException_MissingDefaultCtor()
		{
		    throw new Exception();
		}

		public BadException_MissingDefaultCtor(string message) : base(message)
		{
		}

		public BadException_MissingDefaultCtor(string message, Exception innerException) : base(message, innerException)
		{
		}

		// This constructor is needed for serialization.
		protected BadException_MissingDefaultCtor(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
	
}



