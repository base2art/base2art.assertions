﻿namespace Base2art.Assertions.Features
{
    using System.Reflection;
    using Base2art;
    using NUnit.Framework;
    using Base2art.Assertions.Fixtures;
    
    [TestFixture]
    public class AssertionFeature
    {
        [Test]
        [ExpectedException(typeof(Base2art.AssertionException))]
        public void ShouldVerifyCtorArguments()
        {
            Insist.ThatConstructor<ObjThatThrowsExceptionWhenItemsPresent>().ValidatesNullArguments();
        }
        
        [Test]
        [ExpectedException(typeof(Base2art.AssertionException))]
        public void ShouldVerifyCtorArguments_MissingArgCheck()
        {
            Insist.ThatConstructor<ObjThatMissesAnItemExceptionNull>().ValidatesNullArguments();
        }
        
        [Test]
        public void ShouldVerifyCtorArguments_WithStructs()
        {
            Insist.ThatConstructor<ObjThatHasStructs>().ValidatesNullArguments();
        }
        
        [Test]
        public void ShouldVerifyCtorArguments_MissingArgCheckInterface()
        {
            Insist.ThatConstructor<ObjWithArgCheckingInterface>().ValidatesNullArguments();
        }
        
        [Test]
        [ExpectedException(typeof(Base2art.AssertionException))]
        public void ShouldVerifyCtorArguments_WithStructsInterface()
        {
            Insist.ThatConstructor<ObjWithNoArgCheckingInterface>().ValidatesNullArguments();
        }
        
        [Test]
        public void ShouldVerify()
        {
            Insist.ThatException<Base2art.AssertionException>().IsImplementedCorrectly();
        }
        
        [Test]
        [ExpectedException(typeof(Base2art.AssertionException))]
        public void ShouldVerify1()
        {
            Insist.ThatException<BadException_NotSerializable>().IsImplementedCorrectly();
        }
        
        [Test]
        [ExpectedException(typeof(Base2art.AssertionException))]
        public void ShouldVerify2()
        {
            Insist.ThatException<BadException_MissingDefaultCtor>().IsImplementedCorrectly();
        }
        
        [Test]
        [ExpectedException(typeof(Base2art.AssertionException))]
        public void ShouldVerify3()
        {
            Insist.ThatException<BadException_MissingCtor>().IsImplementedCorrectly();
        }
        
        [Test]
        [ExpectedException(typeof(Base2art.AssertionException))]
        public void ShouldVerify4()
        {
            Insist.ThatException<BadException_MissingCtorOther>().IsImplementedCorrectly();
        }
        
        [Test]
        [ExpectedException(typeof(Base2art.AssertionException))]
        public void ShouldVerify5()
        {
            Insist.ThatException<BadException_MissingCtorSerializer>().IsImplementedCorrectly();
        }
        
        [Test]
        [ExpectedException(typeof(Base2art.AssertionException))]
        public void ShouldVerify6()
        {
            Insist.ThatException<BadException_WrongSerialization>(new BadException_WrongSerialization(){CustomData = 1}).IsImplementedCorrectly();
        }
        
        [Test]
        [ExpectedException(typeof(Base2art.AssertionException))]
        public void ShouldVerify7()
        {
            Insist.ThatException<BadException_WrongSerialization>(new BadException_WrongSerialization(){CustomData = 10}).IsImplementedCorrectly();
        }
        
        [Test]
        [ExpectedException(typeof(Base2art.AssertionException))]
        public void ShouldVerify8()
        {
            Insist.ThatException<BadException_WrongSerialization2>(new BadException_WrongSerialization2(){CustomData = 10}).IsImplementedCorrectly();
        }
        
        [Test]
        [ExpectedException(typeof(Base2art.AssertionException))]
        public void ShouldVerify9()
        {
            Insist.ThatException<BadException_MissingCtorSerializer>().IsImplementedCorrectly();
        }
        
        [Test]
        public void ShouldVerify10()
        {
            Insist.ThatException<Base2art.AssertionException>(new Base2art.AssertionException()).IsImplementedCorrectly();
        }
        
        [Test]
        [ExpectedException(typeof(Base2art.AssertionException))]
        public void ShouldVerifyPropertyArrayNeverNull_Fails()
        {
            var obj =  new BadCustomObj { A = new string[0] };
            obj.Assert().NeverNull(x => x.A, (x, y) => x.A = y);
        }
        
        [Test]
        public void ShouldVerifyPropertyArrayNeverNull_Passes()
        {
            var obj =  new GoodCustomObj { A = new string[0] };
            obj.Assert().NeverNull(x => x.A, (x, y) => x.A = y);
        }
        
        [Test]
        [ExpectedException(typeof(Base2art.AssertionException))]
        public void ShouldVerifyPropertyNeverNull_Fails()
        {
            var obj =  new BadCustomObj { A = new string[0] };
            obj.Assert().NeverNull(x => x.A, (x, y) => x.A = y, () => new string[0]);
        }
        
        [Test]
        public void ShouldVerifyPropertyNeverNull_Passes()
        {
            var obj =  new GoodCustomObj { A = new string[0] };
            obj.Assert().NeverNull(x => x.A, (x, y) => x.A = y, () => new string[0]);
        }
        
        [Test]
        [ExpectedException(typeof(Base2art.AssertionException))]
        public void ShouldVerifyPropertyExpressionArrayNeverNull_Fails()
        {
            var obj =  new BadCustomObj { A = new string[0] };
            obj.Assert().NeverNull(x => x.A);
        }
        
        [Test]
        public void ShouldVerifyPropertyExpressionArrayNeverNull_Passes()
        {
            var obj =  new GoodCustomObj { A = new string[0] };
            obj.Assert().NeverNull(x => x.A);
        }
        
        [Test]
        [ExpectedException(typeof(Base2art.AssertionException))]
        public void ShouldVerifyPropertyExpressionNeverNull_Fails()
        {
            var obj =  new BadCustomObj { A = new string[0] };
            obj.Assert().NeverNull(x => x.A, () => new string[0]);
        }
        
        [Test]
        public void ShouldVerifyPropertyExpressionNeverNull_Passes()
        {
            var obj =  new GoodCustomObj { A = new string[0] };
            obj.Assert().NeverNull(x => x.A, () => new string[0]);
        }
        
        [Test]
        [ExpectedException(typeof(Base2art.AssertionException))]
        public void ShouldVerifyPropertyExpressionNeverNull_FailsWithString()
        {
            var obj =  new BadCustomObjWith<string> { A = null };
            obj.Assert().NeverNull(x => x.A);
        }
        
        [Test]
        public void ShouldVerifyPropertyExpressionNeverNull_PassesWithString()
        {
            var obj =  new GoodCustomObjWith<string>("") { A = null };
            obj.Assert().NeverNull(x => x.A);
        }
        
        [Test]
        [ExpectedException(typeof(Base2art.AssertionException))]
        public void ShouldVerifyPropertyExpressionNeverNull_FailsWithCustomObj()
        {
            var obj =  new BadCustomObjWith<Person> { A = null };
            obj.Assert().NeverNull(x => x.A);
        }
        
        [Test]
        public void ShouldVerifyPropertyExpressionNeverNull_PassesWithCustomObj()
        {
            var obj =  new GoodCustomObjWith<Person>(new Person()) { A = null };
            obj.Assert().NeverNull(x => x.A);
        }
        
        [Test]
        [ExpectedException(typeof(Base2art.AssertionException))]
        public void ShouldVerify_Equality()
        {
            new EqualObject_Correctly().Assert().Equality();
        }
        
        [Test]
        public void ShouldVerify_Equality_ReferenceErrors()
        {
            new EqualObject_BadImplementation().Assert().Equality();
        }
        
        [Test]
        public void ShouldVerify_Equality_ReferenceErrors_HasValues()
        {
            new EqualObject_BadImplementation{ Value = "ABC" }.Assert().Equality<EqualObject_BadImplementation>();
        }
        
        private class BadCustomObj
        {
            public string[] A  { get; set; }
        }
        
        private class BadCustomObjWith<T>
        {
            public T A  { get; set; }
        }
        
        private class GoodCustomObj
        {
            private string[] a;
            public string[] A
            {
                get { return a = (a ?? new string[0]); }
                set { a = value; }
            }
        }
        
        private class GoodCustomObjWith<T>
            where T : class
        {
            private T a;
            private T a_default;
            
            public GoodCustomObjWith()
            {
            }
            
            public GoodCustomObjWith(T a_default)
            {
                this.a_default = a_default;
            }
            
            public T A
            {
                get { return a = (this.a ?? this.a_default); }
                set { a = value; }
            }
        }
        
        private class Person
        {
        }
    }
}