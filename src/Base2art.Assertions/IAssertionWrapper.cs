﻿namespace Base2art
{
    using System;
    using System.Linq.Expressions;
    
    public interface IAssertionWrapper<T>
        where T : class
    {
        void NeverNull<TItem>(
            Expression<Func<T, TItem>> getter)
            where TItem : class;
        
        void NeverNull<TItem>(
            Func<T, TItem> getter,
            Action<T, TItem> setter)
            where TItem : class;
        
        void NeverNull<TItem>(
            Expression<Func<T, TItem>> getter,
            Func<TItem> creator)
            where TItem : class;
        
        void NeverNull<TItem>(
            Func<T, TItem> getter,
            Action<T, TItem> setter,
            Func<TItem> creator)
            where TItem : class;

        void Equality();
        
        void Equality<TEquatible>();
    }
}
