﻿namespace Base2art
{
    using System;
    using System.Runtime.Serialization;

    public interface IAssertionExceptionWrapper<TException>
        where TException : Exception, ISerializable, new()
    {
        void IsImplementedCorrectly();

        TException IsImplementedCorrectly(Func<TException> creationFunction);
    }
    
}
