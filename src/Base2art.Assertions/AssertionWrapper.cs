﻿namespace Base2art
{
    using System;
    using System.Linq.Expressions;
    using System.Reflection;
    using Newtonsoft.Json;
    
    public class AssertionWrapper<T> : IAssertionWrapper<T>
        where T : class
    {
        private readonly T instance;

        public AssertionWrapper(T instance)
        {
            this.instance = instance;
        }

        public void Equality()
        {
            this.Equality<T>();
        }
        
        public void Equality<TEquatible>()
        {
            if (this.instance == null)
            {
                throw new ArgumentNullException("instance");
            }
            
            T @default = default(T);
            
            var param1 = Expression.Parameter(typeof(T), "p1");
            var param2 = Expression.Parameter(typeof(T), "p2");
            
            var compare = Expression.Lambda<Func<T, T, bool>>(Expression.Equal(param1, param2), param1, param2).Compile();
            var compareNegative = Expression.Lambda<Func<T, T, bool>>(Expression.NotEqual(param1, param2), param1, param2).Compile();
            
            var comp1 = compare(@default, instance);
            var comp2 = compare(instance, @default);
            var comp3 = !compareNegative(@default, instance);
            var comp4 = !compareNegative(instance, @default);
            
            if (!compare(@default, @default))
            {
                throw new Base2art.AssertionException("You have implemented equality wrong. (LHS:=null, RHS:=null)");
            }
            
            if (!compare(@default, @default))
            {
                throw new Base2art.AssertionException("You have implemented equality wrong. (LHS:=hasValue, RHS:=hasValue)");
            }
            
            if (comp1)
            {
                throw new Base2art.AssertionException("You have implemented equality wrong. (LHS:=null, RHS:=hasValue)");
            }
            
            if (comp2)
            {
                throw new Base2art.AssertionException("You have implemented equality wrong. (LHS:=hasValue, RHS:=null)");
            }
            
            if (comp3)
            {
                throw new Base2art.AssertionException("You have implemented NotEquality wrong. (LHS:=null, RHS:=hasValue)");
            }
            
            if (comp4)
            {
                throw new Base2art.AssertionException("You have implemented NotEquality wrong. (LHS:=hasValue, RHS:=null)");
            }
            
            var content = JsonConvert.SerializeObject(instance);
            var clone = JsonConvert.DeserializeObject<T>(content);
            
            var compClone1 = compare(clone, instance);
            var compClone2 = compare(instance, clone);
            var compClone3 = !compareNegative(clone, instance);
            var compClone4 = !compareNegative(instance, clone);
            
            if (!compClone1)
            {
                throw new Base2art.AssertionException("You have implemented equality wrong. (LHS:=clone, RHS:=hasValue)");
            }
            
            if (!compClone2)
            {
                throw new Base2art.AssertionException("You have implemented equality wrong. (LHS:=hasValue, RHS:=clone)");
            }
            
            if (!compClone3)
            {
                throw new Base2art.AssertionException("You have implemented NotEquality wrong. (LHS:=clone, RHS:=hasValue)");
            }
            
            if (!compClone4)
            {
                throw new Base2art.AssertionException("You have implemented NotEquality wrong. (LHS:=hasValue, RHS:=clone)");
            }
            
            var compGetHashCode1 = clone.GetHashCode() == instance.GetHashCode();
            
            if (!compGetHashCode1)
            {
                throw new Base2art.AssertionException("You have implemented GetHashCode() wrong. (LHS:=clone, RHS:=hasValue)");
            }
            
            var compInstanceEquals = instance.Equals(clone);
            var compInstanceEqualsNull = instance.Equals(null);
            
            if (!compInstanceEquals)
            {
                throw new Base2art.AssertionException("You have implemented Equals() wrong. (LHS:=instance, RHS:=clone)");
            }
            
            if (compInstanceEqualsNull)
            {
                throw new Base2art.AssertionException("You have implemented Equals() wrong. (LHS:=instance, RHS:=null)");
            }
            
            if (!(instance is IEquatable<TEquatible>))
            {
                throw new Base2art.AssertionException("You have not implemented IEquatable<TEquatible>.");
            }
            
            var compInstanceEqualsGeneric = ((IEquatable<TEquatible>)instance).Equals(clone);
            var compInstanceEqualsGenericInterface = ((IEquatable<TEquatible>)instance).Equals((TEquatible)(object)clone);
            var compInstanceEqualsGenericNull = ((IEquatable<TEquatible>)instance).Equals(null);
            
            if (!compInstanceEqualsGeneric)
            {
                throw new Base2art.AssertionException("You have implemented IEquatable<TEquatible>.Equals(T other). (LHS:=instance, RHS:=clone)");
            }
            
            if (!compInstanceEqualsGenericInterface)
            {
                throw new Base2art.AssertionException("You have implemented IEquatable<TEquatible>.Equals(TEquatible other). (LHS:=instance, RHS:=clone)");
            }
            
            if (compInstanceEqualsGenericNull)
            {
                throw new Base2art.AssertionException("You have implemented IEquatable<TEquatible>.Equals(T other). (LHS:=instance, RHS:=null)");
            }
        }
        
        public void NeverNull<TItem>(
            Expression<Func<T, TItem>> getter)
            where TItem : class
        {
            var prop = (PropertyInfo)((MemberExpression)getter.Body).Member;
            Action<T, TItem> setter = (x, y) => prop.SetValue(this.instance, y, null);
            
            this.NeverNull(getter.Compile(), setter);
        }
        
        public void NeverNull<TItem>(
            Func<T, TItem> getter,
            Action<T, TItem> setter)
            where TItem : class
        {
            if (typeof(TItem).IsArray)
            {
                var newGetter = new Func<T, object[]>(x => (object[])(object)getter(x));
                var newSetter = new Action<T, object[]>((x, y) => setter(x, (TItem)(object)y));
                
                this.AssertNeverNullForArray(newGetter, newSetter, typeof(TItem).GetElementType());
            }
            else
            {
                if (typeof(TItem) == typeof(string))
                {
                    this.NeverNull(getter, setter, () => (TItem)(object)"s");
                }
                else
                {
                    this.NeverNull(getter, setter, () => JsonConvert.DeserializeObject<TItem>("{}"));
                }
            }
        }
        
        public void NeverNull<TItem>(Expression<Func<T, TItem>> getter, Func<TItem> creator) where TItem : class
        {
            var prop = (PropertyInfo)((MemberExpression)getter.Body).Member;
            Action<T, TItem> setter = (x, y) => prop.SetValue(this.instance, y, null);
            
            this.NeverNull(getter.Compile(), setter, creator);
        }
        
        public void NeverNull<TItem>(
            Func<T, TItem> getter,
            Action<T, TItem> setter,
            Func<TItem> creator)
            where TItem : class
        {
            AssertIf(getter(this.instance), x => x != null, "Item should Not Be Null");
            AssertIf(getter(this.instance), x => object.ReferenceEquals(x, getter(this.instance)), "Item should Be Equal");
            AssertIf(getter(this.instance), x => x != null, "Item should Not Be Null");
            
            setter(this.instance, null);
            
            AssertIf(getter(this.instance), x => x != null, "Item should Not Be Null");
            AssertIf(getter(this.instance), x => object.ReferenceEquals(x, getter(this.instance)), "Item should Be Equal");
            AssertIf(getter(this.instance), x => x != null, "Item should Not Be Null");
            
            setter(this.instance, creator());
            
            AssertIf(getter(this.instance), x => x != null, "Item should Not Be Null");
            AssertIf(getter(this.instance), x => object.ReferenceEquals(x, getter(this.instance)), "Item should Be Equal");
            AssertIf(getter(this.instance), x => x != null, "Item should Not Be Null");
        }

        private static void AssertIf<TItem>(TItem[] item, Func<TItem[], bool> par, string message)
        {
            if (!par(item))
            {
                throw new AssertionException(message);
            }
        }

        private static void AssertIf<TItem>(TItem item, Func<TItem, bool> par, string message)
        {
            if (!par(item))
            {
                throw new AssertionException(message);
            }
        }
        
        private void AssertNeverNullForArray(
            Func<T, object[]> getter,
            Action<T, object[]> setter,
            Type t)
        {
            AssertIf(getter(this.instance), x => x != null, "Item should Not Be Null");
            AssertIf(getter(this.instance), x => x.Length == 0, "Item.Length should be 0");
            
            setter(this.instance, null);
            
            AssertIf(getter(this.instance), x => x != null, "Item should Not Be Null");
            AssertIf(getter(this.instance), x => x.Length == 0, "Item.Length should be 0");
            
            setter(this.instance, (object[])Array.CreateInstance(t, 1));
            
            AssertIf(getter(this.instance), x => x != null, "Item should Not Be Null");
            AssertIf(getter(this.instance), x => x.Length == 1, "Item.Length should be 1");
        }
    }
}
