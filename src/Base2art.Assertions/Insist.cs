﻿namespace Base2art
{
    using System;
    using System.Runtime.Serialization;
    
    public static class Insist
    {
        public static IAssertionWrapper<T> Assert<T>(this T instance)
            where T : class
        {
            return new AssertionWrapper<T>(instance);
        }
        
        public static IAssertionExceptionWrapper<TException> ThatException<TException>()
            where TException : Exception, ISerializable, new()
        {
            return new AssertionExceptionWrapper<TException>();
        }
        
        public static IAssertionExceptionWrapper<TException> ThatException<TException>(TException instance)
            where TException : Exception, ISerializable, new()
        {
            return new AssertionExceptionWrapper<TException>(instance);
        }

        public static IAssertionConstructorWrapper<T> ThatConstructor<T>()
        {
            return new AssertionConstructorWrapper<T>();
        }
    }
}
