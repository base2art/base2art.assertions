﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Base2art.Internals;
namespace Base2art
{
    public class AssertionConstructorWrapper<T> : IAssertionConstructorWrapper<T>
    {
        public void ValidatesNullArguments()
        {
            var ctors = typeof(T).GetConstructors();
            foreach (var ctor in ctors)
            {
                this.ValidateNullArguments(ctor);
            }
        }

        public void ValidateNullArguments(ConstructorInfo ctor)
        {
            var parameters = ctor.GetParameters();
            List<object> objs = new List<object>();
            
            for (int i = 0; i < parameters.Length; i++)
            {
                objs.Add(this.MakeParameter(parameters[i]));
            }
            
            try
            {
                ctor.Invoke(objs.ToArray());
            }
            catch (Exception ex)
            {
                throw new Base2art.AssertionException("You should have a ctor that doesn't throw exceptions when all parameters are not null", ex);
            }
            
            for (int i = 0; i < parameters.Length; i++)
            {
                if (parameters[i].ParameterType.IsValueType)
                {
                    continue;
                }
                
                var items = objs.ToArray();
                
                items[i] = null;
                
                bool hasException = false;
                try
                {
                    ctor.Invoke(items);
                }
                catch (TargetInvocationException ex)
                {
                    if (ex.InnerException is ArgumentException)
                    {
                        hasException = true;
                    }
                }
                
                if (!hasException)
                {
                    throw new Base2art.AssertionException("This Constructor does not validate all Parameters for null");
                }
            }
        }

        private object MakeParameter(ParameterInfo parameterInfo)
        {
            var parameterType = parameterInfo.ParameterType;
            
            if (parameterType == typeof(string))
            {
                return Guid.NewGuid().ToString("N");
            }
            
            if (parameterType.IsValueType)
            {
                return Activator.CreateInstance(parameterType);
            }
            
            var proxy = new CustomProxy(parameterType);
            return proxy.GetTransparentProxy();
        }
    }
}
