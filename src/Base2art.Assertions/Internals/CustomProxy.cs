﻿
namespace Base2art.Internals
{
    using System;
    using System.Reflection;
    using System.Runtime.Remoting.Messaging;
    using System.Runtime.Remoting.Proxies;
    
    public class CustomProxy : RealProxy
    {
        public CustomProxy(Type proxiedType)
            : base(proxiedType)
        {
        }
        
        public override IMessage Invoke(IMessage msg)
        {
            var methodCall = msg as IMethodCallMessage;

            return methodCall != null ? HandleMethodCall(methodCall) : null;

        }
        
        private IMessage HandleMethodCall(IMethodCallMessage methodCall)
        {
            return new ReturnMessage(null, null, 0, methodCall.LogicalCallContext, methodCall);
        }
    }
}

//            try
//            {}
//            catch(TargetInvocationException invocationException)
//            {
//                var exception = invocationException.InnerException;
//                return new ReturnMessage(exception, methodCall);
//            }
