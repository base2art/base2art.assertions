﻿using System.Reflection;

[assembly: AssemblyTitle("Base2art.Assertions")]
[assembly: AssemblyDescription("A Set of utilities that adds some assertive capabilities; particularly around asserting that Exceptions are implemented correctly")]
[assembly: AssemblyConfiguration("")]

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "Base2art.Collections", Justification = "SjY")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "Base2art.IO", Justification = "SjY")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "Base2art.Net", Justification = "SjY")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "Base2art.Security", Justification = "SjY")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "Base2art.Serialization", Justification = "SjY")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "Base2art.Text", Justification = "SjY")]    