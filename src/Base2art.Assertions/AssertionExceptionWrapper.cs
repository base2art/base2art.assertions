﻿namespace Base2art
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Reflection;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Formatters.Binary;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    public class AssertionExceptionWrapper<TException> : IAssertionExceptionWrapper<TException>
        where TException : Exception, ISerializable, new()
    {
        private readonly TException instance;

        public AssertionExceptionWrapper()
        {
        }
        
        public AssertionExceptionWrapper(TException instance)
        {
            this.instance = instance;
        }

        public void IsImplementedCorrectly()
        {
            if (this.instance != null)
            {
                this.IsImplementedCorrectly(
                    () => this.instance);
            }
            
            this.IsImplementedCorrectly(
                () => new TException());
            
            this.IsImplementedCorrectly(
                () => new TException());
            
            this.IsImplementedCorrectly(
                () => (TException)Activator.CreateInstance(typeof(TException), new object[] { "Message Text" }));
            
            this.IsImplementedCorrectly(
                () => (TException)Activator.CreateInstance(typeof(TException), new object[] { "Message Text", new InvalidOperationException("Inner exception.") }));
        }
        
        public TException IsImplementedCorrectly(Func<TException> creationFunction)
        {
            TException ex = null;
            try
            {
                ex = creationFunction();
            }
            catch (TargetInvocationException tie)
            {
                throw new AssertionException("Exception not implemented correctly", tie.InnerException);
            }
            catch (MissingMethodException tie)
            {
                throw new AssertionException("Exception not implemented correctly", tie.InnerException);
            }
            
            try
            {
                throw ex;
            }
            catch (TException te)
            {
                var bf = new BinaryFormatter();
                
                using (var ms = new MemoryStream())
                {
                    try
                    {
                        // "Save" object state
                        bf.Serialize(ms, te);
                    }
                    catch (Exception e)
                    {
                        throw new AssertionException(typeof(TException) + " is not marked as Serializable", e);
                    }

                    // Re-use the same stream for de-serialization
                    ms.Seek(0L, SeekOrigin.Begin);

                    // Replace the original exception with de-serialized one
                    TException result = null;
                    try
                    {
                        result = (TException)bf.Deserialize(ms);
                    }
                    catch (Exception e)
                    {
                        throw new AssertionException(typeof(TException) + " does not have a deserialization constructor", e);
                    }
                    
                    var actual = CleanUp(result);
                    var expected = CleanUp(te);

                    foreach (var prop in typeof(TException).GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance))
                    {
                        if (!actual.Contains(prop.Name))
                        {
                            throw new AssertionException(
                                string.Format(CultureInfo.InvariantCulture, "Missing Serialized Property: '{0}'", prop.Name));
                        }
                    }
                    
                    if (actual.Length != expected.Length)
                    {
                        throw new AssertionException(
                            string.Format(
                                CultureInfo.InvariantCulture, 
                                "actual.length: '{0}' expected.length {1};\r\ndiff:\r\n{2}\r\n{3}",
                                actual.Length,
                                expected.Length,
                                actual,
                                expected));
                    }
                    
                    if (!string.Equals(actual, expected))
                    {
                        throw new AssertionException(
                            string.Format(
                                CultureInfo.InvariantCulture, 
                                "actual.length: '{0}' expected.length {1};\r\ndiff:\r\n{2}\r\n{3}",
                                actual.Length,
                                expected.Length,
                                actual,
                                expected));
                    }
                    
                    return result;
                }
            }
        }
        
        private static string CleanUp(TException ex)
        {
            var serialized = JsonConvert.SerializeObject(ex, Formatting.Indented);
            JObject actualObj = (JObject)JsonConvert.DeserializeObject(serialized);
            actualObj.Property("ExceptionMethod").Value = null;
            return JsonConvert.SerializeObject(actualObj, Formatting.Indented);
        }
    }
}
