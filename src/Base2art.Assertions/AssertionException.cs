﻿namespace Base2art
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public class AssertionException : Exception
    {
        public AssertionException()
        {
        }

        public AssertionException(string message) : base(message)
        {
        }

        public AssertionException(string message, Exception innerException) : base(message, innerException)
        {
        }

        // This constructor is needed for serialization.
        protected AssertionException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}